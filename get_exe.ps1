# Set config variables here
$project = "eu4"
$config = "release_d"
$choco_command = "choco upgrade -y pdsbuild"

# Update pdsbuild
function Test-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}
if ((Test-Admin) -eq $false) {
    Start-Process powershell.exe -Wait -Verb RunAs -ArgumentList ('-noprofile -noexit -command "{0}; exit"' -f ($choco_command))
    RefreshEnv
} else {
    Invoke-Expression $choco_command
    RefreshEnv
}

# Test for presence of git and error out if not present
if ((Get-Command "git" -ErrorAction SilentlyContinue) -eq $null)
{
    echo 'We require command line git to be present'
    exit 1
}

# Test for presence of git and error out if not present
if ((Get-Command "pdsbuild" -ErrorAction SilentlyContinue) -eq $null)
{
    echo 'Choco package pdsbuild still not present, run the bootstrap script again, and then this script again, and then ask for help'
    exit 1
}

# Find name of current branch
$branch = git symbolic-ref --short HEAD
# Deal with branches which have slash characters in them which get replaced with %2F in Samba shares
$branch = $branch.replace('/', 'f')
echo "branch: $branch"

# Check if we are being run in the build folder
$destination = split-path -parent $MyInvocation.MyCommand.Definition
echo "destination: $destination"

$build_key = "jenkins-{0}-{1}-windows-{2}" -f ($project, $config, $branch)
echo "build_key: $build_key"

pdsbuild.exe deployscripts buildsync -s azure -p windows -c debug -b $project pull $build_key --destination $destination -i ${project}-dev --latest True --zip True

pause
